#!/usr/bin/env python
'''
Este archivo se uso para las pruebas de concepto mongodb vs rethinkdb
'''
import pymongo
import json
from datetime import datetime

from threading import Thread

myclient = pymongo.MongoClient("mongodb://mongodb:27017/")
#r = RethinkDB()
#rconn = r.connect( "localhost", 28015)
mydb = myclient["grupos"]
mycol = mydb["vacantes"]
#r.db("grupos").table("vacantes").run(rconn)

def poblar_mongo():
    grupo_mongo = []
    with open("consulta.json","r") as f:
        grupos = f.read()
        gruposJ = json.loads(grupos)
    for grupo in gruposJ:
        grupo["_id"] = grupo["id_grupo_asignatura"]
        grupo_mongo.append(grupo)
    for grupo in grupo_mongo:
        try:
            x = mycol.insert_one(grupo)
        except Exception as e:
            pass



def consultar_mongo():
    res = []
    for x in mycol.find({"id_asignatura":"1121"}):
        res.append(x)
    print(res)


def consulta_time(function):
    start_time = datetime.now()
    function()
    end_time = datetime.now()
    print('Duration: {}'.format(end_time - start_time))

if __name__ == "__main__":
    print("inicio Mongo")
    x = Thread(target=poblar_mongo, args=())
    x.start()
    x.join()
    print("fin Mongo")  
    