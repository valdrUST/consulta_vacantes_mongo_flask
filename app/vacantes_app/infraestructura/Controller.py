#!/usr/bin/env python
import logging
from ..aplicacion import Aplicacion
from .Database import Database
class Controller():
    def __init__(self):
        self.db = Database()
    
    def get_vacante(self,id_materia):
        return self.db.consulta({"id_asignatura":int(id_materia)})
    
    def get_asignatura_grupo(self,id_materia_grupo):
        return self.db.consulta({"id_grupo_asignatura":id_materia_grupo})

    def update_vacante(self,id_materia_grupo,cambio):
        vacante = self.get_asignatura_grupo(id_materia_grupo)
        if(len(vacante)>0):
            new_vacantes = vacante[0]["vacantes"] + cambio
            return self.db.update({"id_grupo_asignatura":id_materia_grupo},{ "$set":{'vacantes':new_vacantes}})
        else:
            return None


    
