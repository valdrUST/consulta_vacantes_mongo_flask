#!/usr/bin/env python
from flask import Flask, escape, request, Blueprint, jsonify, make_response
from os import mkdir, path
import base64
import json
import tempfile
import logging
from flask_socketio import SocketIO, send, emit
from flask_cors import CORS
from threading import Lock
from .Controller import Controller

server_report = Blueprint('app', __name__)
control = Controller()
io = SocketIO()
class Server():
    def __init__(self, name, *args, **kwargs):
        global io
        self.app = Flask(name)
        self.app.register_blueprint(server_report)
        self.cors = CORS(self.app,resources={r"/*":{"origins":"*"}})
        self.io = SocketIO(self.app)
        self.io.init_app(self.app, cors_allowed_origins="*")
        io = self.io
        
def background_thread():
    """Example of how to send server generated events to clients."""
    count = 0
    while True:
        io.sleep(10)
        count += 1
        io.emit('my_response',
                      {'data': 'Server generated event', 'count': count},
                      namespace='/consulta')
        
def proceso(consulta):
    print("base de datos si")
    io.emit('consulta_respuesta',{"data":consulta}, namespace='/consulta')


@server_report.route('/ws/vacantes_app',methods=['GET'])
def index():
    logging.info("index")
    io.emit("kriko",{"data":"hola perro"},namespace='/consulta')
    return "<h1>vacantes_app</h1>"

@server_report.route('/ws/vacantes_app/ocupar_vacante/<id>',methods=['POST'])
def ocupar_vacante(id):
    res = control.update_vacante(id,-1)
    if(res):
        grupo = control.get_asignatura_grupo(id)[0]
        io.emit('vacante_'+id,{"data":grupo}, namespace='/consulta')
    return json.dumps({"status":200})

@server_report.route('/ws/vacantes_app/liberar_vacante/<id>',methods=['POST'])
def liberar_vacante(id):
    res = control.update_vacante(id,1)
    if(res):
        grupo = control.get_asignatura_grupo(id)[0]
        io.emit('vacante_'+id,{"data":grupo}, namespace='/consulta')
    return json.dumps({"status":200})

@server_report.route('/ws/vacantes_app/get_vacante/<id_asignatura>',methods=['GET'])
def get_vacante(id_asignatura):
    lista = []
    vacantes = control.get_vacante(id_asignatura)
    response = json.dumps({"status":200,"data":vacantes})
    return response

@io.on('consulta')
def handle_message(message):
    print(message)
    hola = json.dumps({"hola":"hola"})
    proceso(hola)
