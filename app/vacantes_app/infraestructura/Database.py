#!/bin/python
import pymongo
class Database(object):
    def __init__(self):
        self.client = pymongo.MongoClient("mongodb://mongodb:27017/")
        self.db = self.client["grupos"]
        self.col = self.db["vacantes"]
    
    def consulta(self,query):
        lista = []
        for x in self.col.find(query):
            lista.append(x)
        return lista
    
    def update(self,query,newVal):
        return self.col.update_one(query,newVal).acknowledged