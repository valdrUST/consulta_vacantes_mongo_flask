# consulta de vacantes con mongodb y flask
## Instrucciones de uso
* Iniciar contenedor
```sh
$ docker-compose -f docker-compose.yml up
```
* link a la pagina:
[https://localhost:8081](https://localhost:8081)
## Paquetes usados
* app backend
1) flask
2) socket.io
3) mongodb
* frontend
1) nginx (como reverse proxy)
2) jquery (js/app.js)
3) socket.io (para jquery)
4) datatables
5) bootstrap 4


### notas
* se puede cambiar el puerto modificando el archivo docker-compose.yml
```yml
    client:
        container_name: nginx
        build: ./cliente_web
        restart: always
        ports:
            - 8081:80
        networks:
            - backend
```
* cambiar 8081 por el puerto deseado


