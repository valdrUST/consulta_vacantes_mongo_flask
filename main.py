#!/usr/bin/env python
'''
Este archivo es el que se uso para generar el json haciendo una consulta a la base de datos de desarrollo
'''
import psycopg2
from psycopg2.extras import RealDictCursor
import json

        
conn = psycopg2.connect(database="escolar",user="jvallejo", password="Hola1234!", host="192.168.119.67", port="5432")
conn.autocommit = True
cursor = conn.cursor(cursor_factory=RealDictCursor)
cursor.execute(
    """
    SELECT ga.id_grupo_asignatura,
                   a.id_asignatura,
                   a.nombre AS asignatura,
                   a.nombre_corto AS asignatura_corto,
                   ga.numero,
                   ga.cupo,
                   ga.vacantes,
                   ega.id_estatus,
                   ega.nombre AS estatus,
                   CASE COALESCE(gapi.id_grupo_asignatura, 'NO') WHEN 'NO' THEN 0 ELSE 1 END AS primer_ingreso,
                   gp.rfc AS rfc,
                   CASE WHEN ti.id_titulo = 0 THEN COALESCE(t.titulo_aux || ' ', '') ELSE ti.siglas || ' ' END || p.nombre || ' ' || p.apellido_paterno || ' ' || COALESCE(p.apellido_materno, '') AS nombre_completo,
                   tc.id_tipo_clase,
                   tc.nombre as tipo_clase,
                   tc.abreviado as siglas_tipo_clase,
                   gh.dias,
                   s.id_salon,
                   to_char(gh.hora_inicio,'HH24:MI') as hora_inicio,
                   to_char(gh.hora_fin,'HH24:MI') as hora_fin
            FROM core.grupo_asignatura ga
                LEFT JOIN core.grupo_asignatura_pi gapi ON gapi.id_grupo_asignatura = ga.id_grupo_asignatura
                INNER JOIN core.asignatura a ON a.id_asignatura = ga.id_asignatura
                INNER JOIN core.estatus_grupo_asignatura ega ON ega.id_estatus = ga.id_estatus
                INNER JOIN core.grupo_profesor gp ON gp.id_grupo_asignatura = ga.id_grupo_asignatura
                INNER JOIN core.tipo_clase tc ON tc.id_tipo_clase = gp.id_tipo_clase
                INNER JOIN core.docente d ON d.rfc = gp.rfc
                INNER JOIN core.trabajador t ON t.rfc = d.rfc
                LEFT JOIN core.titulo ti ON ti.id_titulo = t.id_titulo
                INNER JOIN core.persona p ON p.id_persona = t.id_persona
                INNER JOIN core.grupo_horario gh ON gh.id_grupo_profesor = gp.id_grupo_profesor
                LEFT JOIN core.salon s ON s.id_salon = gh.id_salon
            WHERE ga.id_estatus IN (0,1) AND ga.curricular = true
            ORDER BY ga.numero, gp.rfc, gp.id_tipo_clase""")
res = cursor.fetchall()
with open("consulta.json","w") as f:
    j = json.dumps(res)
    f.write(j)
conn.close()
print(j)